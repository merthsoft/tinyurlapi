﻿Merthsoft.TinyUrlApi
Merthsoft Creations, 2015
Shaun McFall

A simple wrapper around the http://tny.im/ URL shortener API, as well as the http://is.gd/ URL shortener API.
For the sake of backward compatibility, I'm keeping everything ins the Merthsoft.TinyUrlApi namespace, and the 
TinyUrl static class.

To use just include Merthsoft.TinyUrlApi and call with TinyUrl.GetTinyUrl("http://yoururl.com") or
TinyUrl.GetIsGdUrl("http://yoururl.com"). 

If it fails to generate a URL (this happens sometimes if you make too many references too quickly),
it returns null. This probably isn't the best way to do it, but it's the easiest for how I use it.
(Maybe it should be an exception? I don't know. I like null coalescing.)