﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Merthsoft.TinyUrlApi {
	public static class TinyUrl {
		private const string TINY_URL_API = "http://tny.im/yourls-api.php?action=shorturl&url={0}&format=simple";
        private const string ISGD_URL_API = "http://is.gd/create.php?format=simple&url={0}";

        public static string GetTinyUrl(string url) {
            return getUrlFromApi(url, TINY_URL_API);
        }

        public static string GetIsGdUrl(string url) {
            return getUrlFromApi(url, ISGD_URL_API);
        }

        private static string getUrlFromApi(string url, string apiUrl) {
            var req = (HttpWebRequest)WebRequest.Create(string.Format(apiUrl, url));
            try {
                using (StreamReader s = new StreamReader(req.GetResponse().GetResponseStream())) {
                    return s.ReadToEnd();
                }
            } catch {
                return null;
            }
        }
    }
}
